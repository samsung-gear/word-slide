
var game  = {

    size: 3,
    cellWidth: 0,
    cellR: 0,
    cellSwift: 0,
    level: 1,
    globalLevel: 1,
    isBlockDblMove: false,
    isFinish: false,
    isStop: false,
    isOver: false,
    word: '',
    isWordStart: false,
    symbolPos: [],
    slideCount: 0,
    isFirst: true,
    notAnswer: 0,
    hintCounter: 10,

    init: function() { 
            game.notAnswer = 0;
            
	    var gameField = $('#game-field');
            var helpField = $('#help-field');
	    gameField.html('');
            helpField.html('');
	    
	    for (var i = 1; i <= game.size; i++) {
		    for (var j = 1; j <= game.size; j++) {
			    var id = (i-1) * 3 + j;
                            var left = (j-1) * game.cellWidth; 
                            var top = (i-1) * game.cellWidth; 

                            var cell = $('<div></div>');
                            cell.addClass('cell');

                            cell.attr('data-number', id);

                            cell.css('left', left + 'px');
                            cell.css('top', top + 'px');

                            cell.appendTo(gameField); 
			    
		    }
	    }
            
            var counter = 0;
            for (var key in config[game.level]) {
                    var word = config[game.level][key]['word'];
                    var index = 0;
                 
                    game.notAnswer++;
                    var helpWord = $('<div></div>');
                    helpWord.addClass('help-word');
                    helpWord.attr('data-number', key);
                    for (var pos in config[game.level][key]['pos']) {
                            var number = config[game.level][key]['pos'][pos];
                            var symbol = word.charAt(index);
                            $(".cell[data-number='" + number +  "']").html(symbol);
                            index++;
                            counter++;
                            
                            var helpSymbol = $('<div></div>');
                            helpSymbol.addClass('help-symbol');
                            helpSymbol.attr('data-symbol', symbol);
                            helpSymbol.attr('data-number', counter);
                            helpSymbol.html('*');
                            helpSymbol.appendTo(helpWord); 

                    }
                    helpWord.appendTo(helpField);
            }
	
	    game.slideCount = 0;
	    game.isFinish = false;
	    game.isStop = false;
            
    },
    
    
    
    win: function(delay) { 
            if (parseInt(game.level) === parseInt(game.globalLevel)){
                    var count = Object.keys(config).length;

                    if (count > game.globalLevel) {
                            game.globalLevel ++;
                            main.saveToStorage();
                    }
            }
                    
            var target = Object.keys(config[game.level]).length
        
	    var count = Object.keys(config).length; 
	    if (count !== parseInt(game.level)) {
		    var reward = $('#reward');
		    reward.removeClass();
		
		    if (game.slideCount <= target) {
			    reward.addClass('reward_3');
                            game.hintCounter++;
                            main.saveToStorage();
		    }
		    else if(game.slideCount >= 3*target) {
			    reward.addClass('reward_1');
		    }
		    else {
			    reward.addClass('reward_2');
		    }
		    $('#solved').html(game.slideCount);
		    $('#target').html(target);
                    

                    setTimeout(function() { 
                        navigation.goToPage('win');
                        $(".stars").css({backgroundSize: 0});
                        $(".stars_left").animate({backgroundSize: '100%'}, {duration: 400, queue: true,});
                        $(".stars_center").delay(100).animate({backgroundSize: '100%'}, {duration: 400, queue: true,});
                        $(".stars_right").delay(200).animate({backgroundSize: '100%'}, {duration: 400, queue: true,});
                        
                        
                    }, delay);
		    
	    }
	    else {
                    setTimeout(function() { navigation.goToPage('gameover')}, delay);
		    game.isOver = true;
		    main.saveToStorage();
	    }
	    
    },
    
    
    subscribeEvents: function() {
	    system.addDoubleTapListener("game-field", main.showStop); 
            system.addTouchStartListener("game-field", game.startWord); 
            system.addTouchEndListener("game-field", game.endWord); 
            system.addTouchMoveListener("game-field", game.slide); 

	    	    
	    $('#return').on('click', main.stopReturn);
	    $('#restart').on('click', main.stopRestart);
	    $('#exit').on('click', main.stopExit);
	    $('#hints').on('click', game.hints);
	    $('.help__ok').on('click', main.closeHelp);
            
            $('#helpInfoText').html(LANG['helpInfoText']);
		    
    },
    
    
    hints: function() {
            if (game.hintCounter <= 0) {
                return;
            }
            
            $(".help-symbol").each(function() {
                    var element = $(this);
                    var symbolOpen = element.html();
                    if (symbolOpen === '*') {
                            var symbol = element.attr('data-symbol');
                            element.html(symbol);
                            game.hintCounter--;
                            main.saveToStorage();
                            $('#helpCounter').html('HINTS: ' + game.hintCounter);
                            return false;
                    } 
            });
    },
    
      
    startWord: function(e) {

            game.isWordStart = true;
            game.word = '';
            game.symbolPos = [];
            
            var cell = game.getCell(e, false);
            if (cell !== null) {
                    game.word += cell.symbol;
                    game.symbolPos.push(cell.number);  
            }
    },
    
    
    endWord: function() {
            for (var i=1; i<= game.size * game.size ; i++) {
                    $(".cell[data-number='" + i +  "']").removeClass('cell-active');  
            }
            
            game.isWordStart = false;
     
            game.calc();
    },
    
    
    
    getCell: function(e, isBorderCheck) {
            var cell = null;
            var col =  parseInt((e.pageX) / game.cellWidth) + 1;
	    var row =  parseInt((e.pageY - game.cellSwift) / game.cellWidth) + 1;
            var number = (row-1) * game.size + col;
            var element = $(".cell[data-number='" + number +  "']");
            
            if (isBorderCheck) {
                    var X = (col-1) * game.cellWidth + 12;
                    var Y = (row-1) * game.cellWidth + 12;

                    if (X > e.pageX || Y >  e.pageY) {
                            return null;
                    }
            }
            
            if( element.length ) {
                
                    element.addClass('cell-active');
                    cell = {symbol: element.html(), number: number};
            }
            
            return cell;
    },
    
    
    slide: function(e) {
            if(!game.isWordStart) {
                return;
            }
            var cell = game.getCell(e, true);
            if (cell === null) {
                    return;
            }
         
            var isExist =  system.in_array( cell.number,  game.symbolPos);
            
            if (isExist) {
                    return;
            }
            
            game.word += cell.symbol;
            game.symbolPos.push(cell.number);
            
       
    },
    
    
    helpFill: function(key) {
        $(".help-word[data-number='" + key +  "'] .help-symbol").each(function() {
                var element = $(this);
                var symbol = element.attr('data-symbol');
                element.html(symbol);
        });     
    },
    

    calc: function() {
            var delay = 0;
            var isVibro;
            if (game.word !== '' && game.symbolPos.length !== 1) {
                      game.slideCount++;
                      isVibro = true;
            }
            for (var key in config[game.level]) {
                    var word = config[game.level][key]['word'];
                    if (game.word === word) {
                        
                            isVibro = false;

                            game.helpFill(key);
                            game.notAnswer--;
                            
                            if (game.notAnswer === 0) {
                                    game.isFinish = true; 
                            }
                            var counter = 0;
                            for(var pos in game.symbolPos) { 
                                    counter ++;
                                    $(".cell[data-number='" + game.symbolPos[pos] +  "']")
                                        .delay(counter * 60)
                                        .animate(
                                            {width: 0, margin: '50px', height: 0, fontSize: 0, lineHeight: '100%'}, {
                                                    duration: 500, 
                                                    queue: true,
                                                    complete: function() { 
                                                            $( this ).remove();   
                                                    }
                                            });  
                
                            }
                            
                            delay = 500+ counter*60;
                            if (game.isFinish) {
                                    game.win(delay);
                            } 
                            else {
                                    var offsetArr = game.fillOffsetArr();
                                    game.downAmination(offsetArr, delay)

                            }
                    }
                
                 
                            
                  
            }
            
           if (isVibro) {
               system.vibration(0, 40);
           }
        },
            
            
        fillOffsetArr: function() {
                var arr = {}
                for(var i=1; i<=9;i++) {
                        arr[i] = 0;
                        var isExistInRemove = system.in_array(i, game.symbolPos);
                        if (!isExistInRemove) {
                                for (var downkey in downPos[i]) {
                                        var value = downPos[i][downkey];
                                        var isDown = system.in_array(value, game.symbolPos);
                                        if (isDown) {
                                                arr[i]++;
                                        }
                                }
                        }
                }
                return arr;
        },
        
        
         downAmination: function(arr, delay) {
                for(var key in arr) { 
                        if (arr[key]!==0) {
                            var offset = arr[key] * game.cellWidth;
                                $(".cell[data-number='" + key +  "']")
                                .delay(delay)
                                .animate(
                                    {top: '+=' + offset + 'px' }, {
                                            duration: 500, 
                                            queue: true,
                                            complete: function() { 
                                                    var key = $( this ).attr('data-number')
                                                    var newNumber = parseInt(key) + 3*parseInt(arr[key]);
                                                    $( this ).attr('data-number', newNumber);   
                                            }
                                    });    
                        }
            }
        },

}

var downPos =  {1: [4,7], 2: [5,8], 3: [6,9], 4: [7], 5: [8], 6: [9]}