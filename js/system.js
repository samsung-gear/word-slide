
    
var system  = {
    
    isBrowser: false,
    
    tizenDetect: function () {
	    if (typeof tizen === 'undefined') {
		    system.isBrowser = true;
	    }
	    else {
		    system.isBrowser = false;
	    }
    },
    
    
    checkRange: function (value, max, min) {

	    if (value > max) {
		    value = value - max;
	    }

	    if (value < min) {
		    value = max - Math.abs(value);
	    } 

	    return value;
    },


    zeroFill: function (number) {
	    var str = number.toString(); 

	    if (str.length === 1) {
		    str = '0' + str;
	    }

	    return str;
    },
    
    
    showElement: function(value, name) {
	    if (value) {
		    $('#' + name).css('display', 'block');
	    }
	    else {
		    $('#' + name).css('display', 'none');
	    }
    },
    
    
    changeClass: function(value, id, class1, class2) {
            if(value) {
                    $('#' + id).removeClass(class1);
                    $('#' + id).addClass(class2);
            }
            else {
                    $('#' + id).removeClass(class2);
                    $('#' + id).addClass(class1);
            }
    },
    
    
    getDate2: function () {
	    var date = {};
	    var currentdate = null;

	    if (!system.isBrowser) {
		    currentdate = tizen.time.getCurrentDateTime(); 
	    } 
	    else {
		    currentdate = new Date();   
        
	    }

	    date.hour       = currentdate.getHours();
	    date.minute     = currentdate.getMinutes();
	    date.day        = currentdate.getDate();
	    date.second     = currentdate.getSeconds();
	    date.month      = currentdate.getMonth();
            date.year       = currentdate.getFullYear();
            date.weekDay    = currentdate.getDay();
            date.time       = new Date().getTime();
	    

	    return date;
    },
    
    
    addDoubleTapListener: function (element_id, callBack) {
	    if (!system.isBrowser) {
		    var touchable = document.getElementById(element_id);
		    touchable.addEventListener("touchstart", function(e) {
			    var current_click_time = (new Date()).getTime();
			    if (main.prevoius_click_time) {
				    var interval = current_click_time - main.prevoius_click_time;
					    if (interval > 100 && interval < 500) {
					    callBack();
				    }
			    }
			    main.prevoius_click_time = current_click_time;
		    }, false);
	    }
	    else {
		    $('#' + element_id).on('dblclick', callBack);
	    }
    },
    
    
    addTouchMoveListener: function (element_id, callBack) {
            var touchable = document.getElementById(element_id);
	    if (!system.isBrowser) {
		    touchable.addEventListener("touchmove", function(e) {
                        var newEvent = {pageX: e.touches[0].pageX, pageY: e.touches[0].pageY};
                        callBack(newEvent);    
                    });
	    }
	    else {
                    touchable.addEventListener("mousemove", function(e) {
                        var newEvent = {pageX: e.pageX, pageY: e.pageY};
                        callBack(newEvent);   
                    });
	    }
    },
    
    
    addTouchEndListener: function (element_id, callBack) {
            var touchable = document.getElementById(element_id);
	    if (!system.isBrowser) {
		    touchable.addEventListener("touchend", function(e) {
                        callBack(e);    
                    });
	    }
	    else {
                    touchable.addEventListener("mouseup", function(e) {
                        var newEvent = {pageX: e.pageX, pageY: e.pageY};
                        callBack(newEvent);   
                    });
	    }
    },
    
    
    addTouchStartListener: function (element_id, callBack) {
            var touchable = document.getElementById(element_id);
	    if (!system.isBrowser) {
		    touchable.addEventListener("touchstart", function(e) {
                        var newEvent = {pageX: e.touches[0].pageX, pageY: e.touches[0].pageY};
                        callBack(newEvent);    
                    });
	    }
	    else {
                    touchable.addEventListener("mousedown", function(e) {
                        var newEvent = {pageX: e.pageX, pageY: e.pageY};
                        callBack(newEvent);   
                    });
	    }
    },
    
    
    
    
    subscribeToScreenChangeEvent: function (callBack) {
	    if (!system.isBrowser) {
		    tizen.power.setScreenStateChangeListener(
			    function(previousState, changedState) {
				    main.currentDeviceMode = changedState;
				    if (main.currentDeviceMode != 'SCREEN_OFF') {
					    callBack();
				    }

			    }
		    );
	    }
    },
    
    
    ambientmodechanged: function (callBack) {
            if (!system.isBrowser) {
                    document.addEventListener("ambientmodechanged", callBack);
           }
    },
    
    in_array: function(needle, haystack) {
            for(var i in haystack) {
                if(haystack[i] == needle) return true;
            }
            return false;
    },
    
    vibration: function (delay, duration) {
		if (delay > 0) {
		    setTimeout(function () {
			    if (typeof navigator.webkitVibrate === 'function') {
			    navigator.webkitVibrate(duration);
			} else {
				navigator.vibrate(duration);
			}
		    }, delay);
		}
		else {
		       if (typeof navigator.webkitVibrate === 'function') {
			    navigator.webkitVibrate(duration);
			} else {
			    navigator.vibrate(duration);
			}
		}
	    },
	    
}