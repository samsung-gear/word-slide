
var navigation  = {
    
	backHandlers: [],
	pageList: [],
	pageHistory: [],
    

	registerPage: function (page) {
		navigation.pageList.push(page);
	},


	registerPages: function () {
		$( ".page" ).each(function() {
			var class_name = $(this).attr('data-page');
			navigation.registerPage(class_name);
		});
	},


	hideAll: function () {
		var page_count = navigation.pageList.length;
		for (var i=0; i<page_count; i++) {
			$('.' + navigation.pageList[i]).css('display', 'none');
		}
	},


	getCurrentPageName: function () {
		var name = '';
		if (navigation.pageHistory.length > 0){
			name = navigation.pageHistory[navigation.pageHistory.length - 1];
		}

		return name;
	},


	goToPage: function (page) {
		if(!navigation.isPageRegistered(page)) {
			return;
		}
		navigation.hideAll();
		$('.' + page).css('display', 'block');
		navigation.pageHistory.push(page);
               
	},


	backPage: function() {
            
		if (navigation.pageHistory.length <=1) {
			return false;
		}

		navigation.pageHistory.pop(); 
		var page = navigation.pageHistory.pop(); 

		navigation.notifyBackHandlers(page);
		
		navigation.goToPage(page); 
    
		return true;
	},


	isPageRegistered: function(page) {
		var result = false;
		var page_count = navigation.pageList.length;
		for (var i=0; i<page_count; i++) {
			if (navigation.pageList[i] === page) {
				result = true;
				break;
			}
		}
		return result;
	},


	subscribeToBackEvents: function(handler, fromPage) {
		navigation.backHandlers.push(
			{
				fromPage: fromPage,
				handler: handler
			}
		);
	},


	unsubscribeFromBackEvents: function(handler, fromPage) {
		var arr = navigation.backHandlers;
		for (i in arr) {
			var elem = arr[i];
			if (elem.handler == handler && elem.fromPage == fromPage) {
				arr.removeElement(elem);
			}
		}
	},


	notifyBackHandlers: function(fromPage) {
		var arr = navigation.backHandlers;
		for (i in arr) {
			var elem = arr[i];
			if (elem.fromPage == fromPage) {
				elem.handler();
			}
		}
	},
	
};


