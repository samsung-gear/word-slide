    
    var win = {
	
	    subscribeEvents: function () {
		    $('#level').on('click',  win.levelSelect);
		    $('#replay').on('click', win.replay);
		    $('#next').on('click', win.next);
	    },
	    
	    
	    replay: function() { 
		    game.init();
		    navigation.backPage();
	    },
	  
	
	    levelSelect: function() { 
		    levels.update();
		    
		    navigation.backPage();
		    navigation.backPage();
		    
		    navigation.goToPage('levels');
	    },
	    
	    
	    next: function() { 
		    game.level ++;
		    game.init();
		    navigation.backPage();
	    },
	    
	    
    }
    
    
