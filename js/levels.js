    
    var levels = {
	
	    currentSlide : 1,
	   
	    
	    subscribeEvents: function () {
		    var slider = document.getElementById("levels-slide");
		    tau.event.enableGesture(slider, new tau.event.gesture.Swipe({orientation: "horizontal"}));
		    slider.addEventListener("swipe", levels.slide);
	    },
	     
	    slide: function (e) {
		
		    if (e.detail.direction === "left" && levels.currentSlide < 9) {
			    levels.currentSlide ++;
		    }
		    
		     if (e.detail.direction === "right" && levels.currentSlide > 1) {
			    levels.currentSlide --;
		    }
		    
		    if (levels.currentSlide <= 9 && levels.currentSlide >= 1) {
			    var left = 0 - 100 * (levels.currentSlide - 1);
			    $("#levels-slide").animate({left: left + '%'}, 200, "linear");
			    
			    var title =  $("#levels-title");
			    title.removeClass();
			    title.addClass("levels-title-" + levels.currentSlide)
		    }
	    },
	
	    init: function () {
		    var levelsWrapper = $('#levels-slide');

                    var offsetV = 82;
		    var offsetH = 82;
		    if ($(window).width() === 360) {
			      offsetV = 100;
                              offsetH = 80;
		    }
		
		    var levelsGroup = $('<div class="lelevs-group " id="levesGroup-1"></div>');
		    levelsGroup.addClass('lelevs-group');
		    levelsGroup.attr('id', 'levesGroup-1');
		    var row = 0;
		    var col = 0;
		    
		    var groupCount = 1;
		
		    for(var i in config) { 
			
			    var level = $('<div></div>');
			    
			    var top = row * offsetH;
			    var left = 45 + col * offsetV;
			    level.css('left', left + 'px');
			    level.css('top', top + 'px');

			    level.addClass('level-btn');

			    level.attr('data-number', i);

			    level.appendTo(levelsGroup); 

			    col++;
			    if (i % 3 === 0) {
				    row ++;
				    col = 0;
			    }
			    
			    if (i >= 9 && i % 9 === 0) {
				    groupCount ++;
				    row = 0;
				    col = 0;
				    levelsGroup.appendTo(levelsWrapper); 
				    levelsGroup = $('<div></div>');
				    levelsGroup.addClass('lelevs-group');
				    levelsGroup.attr('id', 'levesGroup-' + groupCount);
			    }
		    }
		    
		    levelsGroup.appendTo(levelsWrapper);
		    
		    
	    },
	    
	    
	    update: function () {
		    for(var i in config) { 
			    var level = $(".level-btn[data-number='" + i +  "']");

			    level.removeClass();
			    level.addClass('level-btn');
			    level.off("click");

			    if (parseInt(i) <= parseInt(game.globalLevel)) {
				    level.addClass('level-open');
				    level.on("click", levels.start);
				    level.html(i);
			    }
			    else {
				    level.addClass('level-lock'); 
			    }
		    }
	    },
	    
	   
	   start: function () {
		    game.level = parseInt($(this).attr('data-number'));
		    if (game.isFirst) {
			    $('.help').css('display', 'table');
			    game.isFirst = false;
			    main.saveToStorageFirst();
		    }
		    game.init();
		    navigation.backPage(); 
		    navigation.goToPage('game');
	   }

	    
	    
    }
    
    
