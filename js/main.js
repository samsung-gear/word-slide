    
  
    $(window).load(function(){
	   
     
            system.tizenDetect();

	    main.subscribeEvents();
	    main.loadFromStorage();
	    main.init();
            
	    navigation.registerPages();
	    navigation.goToPage('main');

	    levels.init();
	    
	    game.subscribeEvents();
	    levels.subscribeEvents();
	    
	    win.subscribeEvents();
            
            localStorage.clear();
            
    });
    
    
    var main = {
	
	    subscribeEvents: function () {
		    main.swipe();
		    $('#play').on('click', main.play);
		    $('#showLevels').on('click', main.showLevels);
	    },
	    
	    
	    init: function () {
		    var width = $('#game-field').width();
		    var height = $(window).height();
		    
		    game.cellWidth = parseInt(width / 3),
		    game.cellR = parseInt(game.cellWidth / 2);
	      
		    game.cellSwift = parseInt((height - width) / 2);
		    
		    main.preloadGear(); 
		    
	    },
	    
	    
	    play: function () {
		    if (game.isFirst) {
			    $('.help').css('display', 'table');
			    game.isFirst = false;
			    main.saveToStorageFirst();
		    }

		    game.level = game.globalLevel;
		    if (game.isOver === true) {
			    game.level = 1;
		    }

		    game.init();
		    navigation.goToPage('game');
	    },
	    
	    showLevels: function () {
		    levels.update();
		    navigation.goToPage('levels');
	    },
	    
	    
	    swipe: function () {
		    document.addEventListener('tizenhwkey', function(e) {
			if(e.keyName == "back") {
			    
				var currentPageName = navigation.getCurrentPageName();
				
				switch (currentPageName) {
					case 'main':
						tizen.application.getCurrentApplication().exit();
						break;
					case 'win':
						navigation.backPage(); 
						navigation.backPage(); 
						break;
					case 'levels':
						navigation.backPage();   
						break;
					case 'stop':
						navigation.backPage();   
						break;
					case 'gameover':
						navigation.backPage(); 
						navigation.backPage(); 
						break;
				}
			}
			    
		    });
	    }, 
	    
	    
	    saveToStorage: function() {
		    localStorage.setItem('level', game.globalLevel);
		    localStorage.setItem('isOver', game.isOver);
                    localStorage.setItem('hintcounter', game.hintCounter);
	    },
	    
	    saveToStorageFirst: function() {
		    localStorage.setItem('isFirst', game.isFirst);
	    },


	    loadFromStorage: function() {
               
		    var globalLevel = localStorage.getItem('level');
		    if (globalLevel != null) {
			    game.globalLevel = parseInt(globalLevel);
		    }
		    
		    var isOver = localStorage.getItem('isOver');
		    if (isOver != null) {
			    game.isOver = $.parseJSON(isOver);
		    }
		    
		    var isFirst = localStorage.getItem('isFirst');
		    if (isFirst != null) {
			    game.isFirst = false;
		    }
		    
		    var hintCounter = localStorage.getItem('hintcounter');
		    if (hintCounter != null) {
			    game.hintCounter = $.parseJSON(hintCounter);
		    }
	    },
	    
	    
	    showStop: function() {
		    if(!game.isFinish) {
			    $('#levelNumber').html('Level ' + game.level);
                            $('#helpCounter').html('HINTS: ' + game.hintCounter);
			    navigation.goToPage('stop');
		    }
	    },
	    
	    
	    stopExit: function() {
		    navigation.backPage(); 
		    navigation.backPage(); 
	    },
	    
	    
	    changeVibro: function() {
		    game.isVibro = !game.isVibro;
		    
		    localStorage.setItem('isVibro', game.isVibro);

		    if (game.isVibro === true) {
			    main.vibration(0, 50);
		    }
		    
		    main.setVibroSwith(game.isVibro);
	    },
	    
	    
	    setVibroSwith: function(isValue) {
		    var vibroBtn = $('#isVibro');

		    vibroBtn.removeClass();
		    if (isValue === true) {
			    vibroBtn.addClass('on');
		    }
		    else {
			    vibroBtn.addClass('off');
		    }
		
	    },
	    
	    vibration: function (delay, duration) {
		if (delay > 0) {
		    setTimeout(function () {
			    if (typeof navigator.webkitVibrate === 'function') {
			    navigator.webkitVibrate(duration);
			} else {
				navigator.vibrate(duration);
			}
		    }, delay);
		}
		else {
		       if (typeof navigator.webkitVibrate === 'function') {
			    navigator.webkitVibrate(duration);
			} else {
			    navigator.vibrate(duration);
			}
		}
	    },
	    
	    
	    
	    stopReturn: function() {
		    navigation.backPage(); 
	    },
	    
	    
	    stopRestart: function() {
		    game.init(); 
		    navigation.backPage(); 
	    },
	    
	    closeHelp: function() {
		    $('.help').css('display', 'none');
	    },
		
	    
	    addDoubleTapListener: function (element_id, callBack) {
		    
		    var touchable = document.getElementById(element_id);
		    touchable.addEventListener("touchstart", function(e) {
			    var current_click_time = (new Date()).getTime();
			    var current_x = e.touches[0].pageX;
			    var current_y = e.touches[0].pageY;
			    if (main.prevoius_click_time) {
				    var d = Math.sqrt((main.prevoiusX - current_x)*(main.prevoiusX - current_x) +  (main.prevoiusY - current_y)*(main.prevoiusY - current_y));
				    d = parseInt(d); 
				    var interval = current_click_time - main.prevoius_click_time;
					    if (interval > 100 && interval < 400 && d < 50) {
					    callBack();
				    }
			    }
			    main.prevoius_click_time = current_click_time;
			    main.prevoiusX = current_x;
			    main.prevoiusY = current_y;
		    }, false);
	    },
	    
	    preloadImages: function(images){
		images.forEach(function(src){
			var img = new Image();
			img.src = src;
		});
	},
	
	preloadGear: function(){ 
		main.preloadImages([
		    './img/stop/caption.png',
		    './img/stop/exit.png',
		    './img/stop/logo.png',
		    './img/stop/return.png',
		    './img/stop/switcher-off.png',
		    './img/stop/switcher-on.png',
		    './img/stop/title.png',
		    './img/stop/vibro-title.png',
		    './img/levels/lock.png',
		    './img/levels/open.png',
		    './img/levels/title.png',
		    
		    './img/win/level.png',
		    './img/win/next.png',
		    './img/win/replay.png',
		    './img/win/stars.png',
                    './img/s-game/cell1.png',
                    './img/s-game/cell2.png'
		]);
	},
	

	
    }
    
    
    window.requestAnimationFrame = window.requestAnimationFrame ||
	window.webkitRequestAnimationFrame ||
	window.mozRequestAnimationFrame ||
	window.oRequestAnimationFrame ||
	window.msRequestAnimationFrame ||
	function (callback) {
		window.setTimeout(callback, 10);
	};
    
