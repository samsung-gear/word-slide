var config  =  {  
    
	// ***********************  LEVEL *********************** 
	1 : {
	    0: {word: 'boy', pos: [1,2,3]},
            1: {word: 'friend', pos: [4,5,6,9,8,7]}
                    
	},
        
        2 : {
	    0: {word: 'moon', pos: [1,4,7,8]},
            1: {word: 'light', pos: [2,5,9,6,3]}
                    
	},
        
        3 : {
	    0: {word: 'school', pos: [1,4,7,8,5,2]},
            1: {word: 'bus', pos: [3,6,9]}
                    
	},
        
        4 : {
	    0: {word: 'blue', pos: [1,2,3,6]},
            1: {word: 'berry', pos: [4,7,8,9,5]}
                    
	},
        
        5 : {
	    0: {word: 'class', pos: [1,4,7,5,2]},
            1: {word: 'room', pos: [3,6,9,8]}          
	},
        
        6: {
	    0: {word: 'beach', pos: [1,2,3,5,6]},
            1: {word: 'sand', pos: [7,4,8,9]}
                    
	},
        
        7 : {
	    0: {word: 'foot', pos: [4,1,2,3]},
            1: {word: 'print', pos: [5,7,8,9,6]}
                    
	},
        
        8 : {
	    0: {word: 'hot', pos: [7,8,9]},
            1: {word: 'summer', pos: [3,2,1,4,5,6]}         
	},
        
        9 : {
	    0: {word: 'hand', pos: [7,8,9,6]},
            1: {word: 'shake', pos: [5,4,1,2,3]}        
	},
        
        10 : {
	    0: {word: 'jelly', pos: [1,2,3,6,5]},
            1: {word: 'bean', pos: [9,8,4,7]}        
	},
        
        11 : {
	    0: {word: 'love', pos: [1,4,5,6]},
            1: {word: 'cupid', pos: [9,8,7,2,3]}        
	},
        
        12 : {
	    0: {word: 'cat', pos: [1,4,7]},
            1: {word: 'kitten', pos: [2,5,8,9,6,3]}        
	},
        
        13 : {
	    0: {word: 'snow', pos: [1,2,3,5]},
            1: {word: 'flake', pos: [9,6,8,7,4]}        
	},
        
        14 : {
	    0: {word: 'small', pos: [1,2,5,6,3]},
            1: {word: 'talk', pos: [9,8,4,7]}        
	},
        
        15 : {
	    0: {word: 'honey', pos: [1,4,7,8,9]},
            1: {word: 'moon', pos: [5,6,3,2]}        
	},
        
        16 : {
	    0: {word: 'every', pos: [9,8,7,5,6]},
            1: {word: 'body', pos: [4,1,2,3]}        
	},
        
        17 : {
	    0: {word: 'fun', pos: [1,2,3]},
            1: {word: 'simple', pos: [4,5,9,6,8,7]}        
	},
        
        18 : {
	    0: {word: 'bee', pos: [1,4,7]},
            1: {word: 'flower', pos: [2,5,9,8,6,3]}        
	},
        
        19 : {
	    0: {word: 'gun', pos: [1,2,3]},
            1: {word: 'powder', pos: [4,7,5,9,8,6]}        
	},
        
        20 : {
	    0: {word: 'holly', pos: [1,2,3,6,5]},
            1: {word: 'wood', pos: [9,8,4,7]}        
	},
        
        21 : {
	    0: {word: 'bubble', pos: [1,4,7,8,5,2]},
            1: {word: 'gum', pos: [3,6,9]}        
	},
        
        22 : {
	    0: {word: 'shell', pos: [1,4,7,8,5]},
            1: {word: 'fish', pos: [2,9,6,3]}        
	},
        
        23 : {
	    0: {word: 'life', pos: [1,2,3,6]},
            1: {word: 'style', pos: [4,7,8,9,5]}        
	},
        
        24 : {
	    0: {word: 'fire', pos: [1,2,3,6]},
            1: {word: 'works', pos: [9,8,7,5,4]}        
	},
        
        25 : {
	    0: {word: 'soda', pos: [7,4,8,9]},
            1: {word: 'water', pos: [1,2,5,6,3]}        
	},
        
        26 : {
	    0: {word: 'fat', pos: [1,2,5]},
            1: {word: 'skinny', pos: [4,7,8,9,6,3]}        
	},
        
        27 : {
	    0: {word: 'ham', pos: [4,5,6]},
            1: {word: 'burger', pos: [7,8,9,3,2,1]}        
	},
        
        28 : {
	    0: {word: 'hour', pos: [4,7,8,5]},
            1: {word: 'glass', pos: [1,2,9,6,3]}        
	},
        
        29 : {
	    0: {word: 'story', pos: [4,5,6,8,7]},
            1: {word: 'book', pos: [1,2,3,9]}        
	},
        
        30 : {
	    0: {word: 'space', pos: [1,2,3,6,5]},
            1: {word: 'ship', pos: [9,8,4,7]}        
	},
        
        31 : {
	    0: {word: 'deep', pos: [1,4,7,8]},
            1: {word: 'sleep', pos: [2,3,6,9,5]}        
	},
        
        32 : {
	    0: {word: 'honey', pos: [1,2,3,5,7]},
            1: {word: 'comb', pos: [4,8,9,6]}        
	},
        
        33 : {
	    0: {word: 'good', pos: [7,4,5,6]},
            1: {word: 'night', pos: [1,2,9,8,3]}        
	},
        
        34 : {
	    0: {word: 'dragon', pos: [1,2,3,5,4,7]},
            1: {word: 'fly', pos: [8,9,6]}        
	},
        
        35 : {
	    0: {word: 'pine', pos: [1,4,5,6]},
            1: {word: 'apple', pos: [7,8,9,2,3]}        
	},
        
        36 : {
	    0: {word: 'super', pos: [1,2,3,6,9]},
            1: {word: 'star', pos: [4,7,5,8]}        
	},
        
        37 : {
	    0: {word: 'life', pos: [1,2,3,5]},
            1: {word: 'guard', pos: [4,7,8,6,9]}        
	},
        
        38 : {
	    0: {word: 'little', pos: [2,3,6,5,9,8]},
            1: {word: 'bug', pos: [1,4,7]}        
	},
        
        39 : {
	    0: {word: 'black', pos: [1,2,3,6,5]},
            1: {word: 'list', pos: [4,7,8,9]}        
	},
        
        40 : {
	    0: {word: 'water', pos: [1,4,5,8,7]},
            1: {word: 'fall', pos: [2,9,6,3]}        
	},
        
        41 : {
	    0: {word: 'fire', pos: [1,2,5,4]},
            1: {word: 'place', pos: [3,6,9,8,7]}        
	},
        
        42 : {
	    0: {word: 'break', pos: [1,2,5,6,3]},
            1: {word: 'fast', pos: [7,4,8,9]}        
	},
        
        43 : {
	    0: {word: 'play', pos: [4,5,6,3]},
            1: {word: 'group', pos: [9,2,1,8,7]}        
	},
        
        44 : {
	    0: {word: 'tiny', pos: [1,4,8,7]},
            1: {word: 'giant', pos: [2,3,6,5,9]}        
	},
        
        45 : {
	    0: {word: 'coffee', pos: [1,2,3,6,5,9]},
            1: {word: 'pot', pos: [4,7,8]}        
	},
        
        46 : {
	    0: {word: 'curry', pos: [1,2,6,5,3]},
            1: {word: 'rice', pos: [7,4,8,9]}        
	},
        
        47 : {
	    0: {word: 'lamb', pos: [7,8,9,6]},
            1: {word: 'sheep', pos: [3,5,4,1,2]}        
	},
        
        48 : {
	    0: {word: 'grape', pos: [7,8,9,6,5]},
            1: {word: 'wine', pos: [3,2,4,1]}        
	},
        
        49 : {
	    0: {word: 'poison', pos: [1,2,3,5,7,4]},
            1: {word: 'gas', pos: [8,6,9]}        
	},
        
        50 : {
	    0: {word: 'super', pos: [4,5,6,9,8]},
            1: {word: 'hero', pos: [1,7,2,3]}        
	},
        
        51 : {
	    0: {word: 'news', pos: [7,8,9,5]},
            1: {word: 'paper', pos: [1,4,2,3,6]}        
	},
        
        52 : {
	    0: {word: 'burn', pos: [4,7,8,5]},
            1: {word: 'smoke', pos: [3,6,9,2,1]}        
	},
        
        53 : {
	    0: {word: 'after', pos: [1,2,4,7,8]},
            1: {word: 'noon', pos: [3,6,9,5]}        
	},
        
        54 : {
	    0: {word: 'chop', pos: [6,3,2,1]},
            1: {word: 'steak', pos: [4,5,7,8,9]}        
	},
        
        55 : {
	    0: {word: 'wash', pos: [1,2,6,3]},
            1: {word: 'house', pos: [4,5,9,8,7]}        
	},
        
        56 : {
	    0: {word: 'child', pos: [1,2,5,4,7]},
            1: {word: 'care', pos: [3,6,9,8]}        
	},
        
        57 : {
	    0: {word: 'some', pos: [1,4,7,8]},
            1: {word: 'thing', pos: [3,2,9,5,6]}        
	},
        
        58 : {
	    0: {word: 'out', pos: [4,5,6]},
            1: {word: 'number', pos: [1,2,3,9,8,7]}        
	},
        
        59 : {
	    0: {word: 'snow', pos: [3,2,1,4]},
            1: {word: 'board', pos: [5,9,6,8,7]}        
	},
        
        60 : {
	    0: {word: 'world', pos: [1,2,5,6,3]},
            1: {word: 'wide', pos: [9,8,7,4]}        
	},
        
        61 : {
	    0: {word: 'stone', pos: [1,2,3,6,5]},
            1: {word: 'wall', pos: [9,8,7,4]}        
	},
        
        62 : {
	    0: {word: 'farm', pos: [1,2,5,6]},
            1: {word: 'house', pos: [4,7,8,9,3]}        
	},
        
        63 : {
	    0: {word: 'top', pos: [4,5,6]},
            1: {word: 'flight', pos: [1,2,3,9,8,7]}        
	},
        
        64 : {
	    0: {word: 'finger', pos: [1,2,5,4,7,8]},
            1: {word: 'tip', pos: [9,6,3]}        
	},
        
        65 : {
	    0: {word: 'jack', pos: [5,9,6,3]},
            1: {word: 'fruit', pos: [2,1,4,7,8]}        
	},
        
        66 : {
	    0: {word: 'spider', pos: [1,2,3,6,5,9]},
            1: {word: 'man', pos: [4,7,8]}        
	},
        
        67 : {
	    0: {word: 'jelly', pos: [7,4,1,2,3]},
            1: {word: 'fish', pos: [5,6,9,8]}        
	},
        
        68 : {
	    0: {word: 'lemon', pos: [1,2,5,6,9]},
            1: {word: 'tree', pos: [4,7,8,3]}        
	},
        
        69 : {
	    0: {word: 'fire', pos: [1,4,5,9]},
            1: {word: 'woods', pos: [2,3,6,8,7]}        
	},
        
        70 : {
	    0: {word: 'first', pos: [4,7,8,5,3]},
            1: {word: 'love', pos: [1,2,6,9]}        
	},
        
        71 : {
	    0: {word: 'movie', pos: [4,5,6,9,8]},
            1: {word: 'film', pos: [3,2,7,1]}        
	},
        
        72 : {
	    0: {word: 'fairy', pos: [1,4,7,8,9]},
            1: {word: 'tale', pos: [6,3,2,5]}        
	},
        
        73 : {
	    0: {word: 'rock', pos: [6,3,2,1]},
            1: {word: 'stars', pos: [4,5,7,8,9]}        
	},
        
        74 : {
	    0: {word: 'beach', pos: [9,6,3,2,1]},
            1: {word: 'ball', pos: [4,7,8,5]}        
	},
        
        75 : {
	    0: {word: 'stop', pos: [1,4,7,5]},
            1: {word: 'watch', pos: [2,9,8,6,3]}        
	},
        
        76 : {
	    0: {word: 'race', pos: [7,8,9,6]},
            1: {word: 'horse', pos: [4,1,2,3,5]}        
	},
        
        77 : {
	    0: {word: 'coin', pos: [1,2,3,5]},
            1: {word: 'purse', pos: [6,9,8,7,4]}        
	},
        
        78 : {
	    0: {word: 'north', pos: [1,2,4,7,5]},
            1: {word: 'pole', pos: [9,8,6,3]}        
	},
        
        79 : {
	    0: {word: 'hat', pos: [2,3,6]},
            1: {word: 'gloves', pos: [1,4,7,8,9,5]}        
	},
        
        80 : {
	    0: {word: 'body', pos: [3,2,5,1]},
            1: {word: 'guard', pos: [4,7,8,6,9]}        
	}
        
//        0 : {
//	    0: {word: '', pos: []},
//            1: {word: '', pos: []}        
//	},
//	
	
	
}

LANG = {
    'helpInfoText': "You can open game help by double-tapping the screen"
}